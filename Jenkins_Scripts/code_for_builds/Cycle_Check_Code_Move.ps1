$time = (Get-Date).tostring("HH")
$cycle = 13, 14
$location = (Get-Location).path
#$share = \\cifs\share\on\unix
#while ($time -eq $cycle)
#{
 #   write-host 'Cycle is running. Cannot move code.'
#}


if ($time -eq $cycle)
{
    Write-Host #################################################
    write-host # Cycle is currently running. Cannot move code. #
    Write-Host #################################################
}
else
{
    Write-Host #############################################
    write-host # Cycle is not running. Code will now move! #
    Write-Host #############################################
    Write-Host Current Working directory is $location
    #xcopy $location\*.* $share | out-host
}